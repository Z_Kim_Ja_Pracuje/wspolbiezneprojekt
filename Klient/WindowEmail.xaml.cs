﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klient
{
    /// <summary>
    /// Interaction logic for WindowEmail.xaml
    /// </summary>
    public partial class WindowEmail : Window
    {

        public WindowEmail()
        {
            InitializeComponent();
        }

        public string[] Emails;

        public void GetEmails(string[] emails)
        {
            Emails = emails;

            ObservableCollection<string> items = new ObservableCollection<string>();
            emails.ToList().ForEach(x => items.Add(x));

            ComboBoxEmailList.ItemsSource = items;
        }

        private void SendEmail(object sender, RoutedEventArgs e)
        {
            string to = ComboBoxEmailList.Text;
            string subject = TextBoxSubject.Text;
            string body = TextBoxBody.Text;


            SmtpClient client = new SmtpClient();
            MailAddress addressFrom = new MailAddress("");
            MailAddress addressTo = new MailAddress(to);

            MailMessage message = new MailMessage(addressFrom, addressTo);
            message.Body = body;
            // message.BodyEncoding = Encoding.UTF8;
            message.Subject = subject;
            client.Send(message);

            // info że wysłane
        }
    }
}
