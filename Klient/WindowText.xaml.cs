﻿using Library;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klient
{
    /// <summary>
    /// Interaction logic for WindowText.xaml
    /// </summary>
    public partial class WindowText : Window
    {
        public WindowText()
        {
            InitializeComponent();
        }

        private string Model { get; set; } // dane wyciągnięte z pliku
        private string ModelName { get; set; } // nazwa pliku
        private Dictionary<int,string> Changes { get; set; } // zmiany, w sumie to nie wiem po co tutaj zadleklarowałem

        public void Init(SendingMethod sending)
        {
            Send = sending;
            Changes = new Dictionary<int, string>();
        }


        public delegate void SendingMethod(ModelChanges ch);
        SendingMethod Send;

        public void ReceiveModel(string[] data)
        {
            string model = "";
            string filename = "";
            string info = "";

            if(data.Length == 3) {
                filename = data[0];
                info = data[1];
                model = data[2];
            }
            else if(data.Length == 2) {
                filename = data[0];
                model = data[1];
            }

            if (!TextBoxModel.Dispatcher.CheckAccess()) {
                Dispatcher.Invoke(() => {
                    TextBoxModel.Text = model;
                    Model = model;
                    ModelName = filename;
                    if (!string.IsNullOrEmpty(info)) {
                        TextBlockInfo.Text = info;
                    }

                }, System.Windows.Threading.DispatcherPriority.Normal);
            }
            else {
                TextBoxModel.Text = model;
                Model = model;
                ModelName = filename;
                if (!string.IsNullOrEmpty(info)) {
                    TextBlockInfo.Text = info;
                }
            }
        }

        public string ReceiveChanges(ModelChanges changes) {
            if (changes.Filename != ModelName) {
                Dispatcher.Invoke(() => TextBlockInfo.Text = $"{changes.UserName} made changes in file {changes.Filename}."); // kto, co, po co
                return "";
            }


            var changed = changes.Changes; // JsonConvert.DeserializeObject<Dictionary<int, string>>(changes.Changes);
            var splittedModel = Model.Split('\n');

            foreach (var c in changed) {
                splittedModel[c.Key] = c.Value;
            }

            Model = splittedModel.Aggregate((curr, next) => $"{curr}\n{next}");

            Action toInvoke = () => {
                TextBoxModel.Text = Model;
                string info = string.Join("", changed.Select(x => $"Line {x.Key} : {x.Value}"));
                TextBlockInfo.Text = $"{changes.UserName} made changes in file {changes.Filename}. Loaded changes: " +
                    $"{Environment.NewLine}{info}";
            };

            Dispatcher.Invoke(toInvoke,
                System.Windows.Threading.DispatcherPriority.Normal
            );

            return Model;

        }

        private void SendChanges(object sender, RoutedEventArgs e)
        {
            // pytanie co tutaj? porównanie linia po linii?
            // albo inaczej - przy każdym evencie Edit na textboxie zapisywać numer linii
            // przy wysyłaniu wyczyścić listę i elo
            // pytanie co przy pobieraniu

            string changed = TextBoxModel.Text;
            string[] changedArray = changed.Split('\n');
            string[] modelArray = Model.Split('\n');

            if (changedArray.Length != modelArray.Length) {
                // coś nie tak, nie powinno się dać wklepywać enterów
            }

            for (int i = 0; i < modelArray.Length; i++) {
                if (modelArray[i] != changedArray[i]) {
                    Changes[i] = changedArray[i];
                }
            }

            ModelChanges ch = new ModelChanges();
            ch.Changes = Changes;
            ch.Filename = ModelName;

            Send(ch);
            // przetestować jeszcze, czy nie lepiej będzie wysłać jsona
            Changes.Clear();


        }

        public void SetLabelInfo(string info, bool error = true) {
            Dispatcher.Invoke(() => {
                TextBlockInfo.Text = info;
                if (error) TextBlockInfo.Foreground = Brushes.Red;
                else TextBlockInfo.Foreground = Brushes.Black;
            });
        }

        private void TextBoxModel_TextChanged(object sender, TextChangedEventArgs e) {
            //string changed = TextBoxModel.Text;
            //string[] changedArray = changed.Split('\n');
            //string[] modelArray = Model.Split('\n');

            //if(changedArray.Length != modelArray.Length) {
            //    // coś nie tak, nie powinno się dać wklepywać enterów
            //}

            //for(int i = 0; i < modelArray.Length; i++) {
            //    if(modelArray[i] != changedArray[i]) {
            //        Changes[i] = changedArray[i];
            //    }
            //}
        }

        private void SaveFile(object sender, RoutedEventArgs e)
        {
            string textToSend = TextBoxModel.Text;
            string date = DateTime.Now.ToString("d.M.yyyy.HH.mm.ss");
            

            string path = @"..\" + date + "_" + ModelName;
            
            if (!File.Exists(path))
            {
                using (File.Create(path));
                StreamWriter sw = new StreamWriter(path);
                sw.Write(textToSend);
                sw.Close();
            }
            else if (File.Exists(path))
            {
                using (var sw = new StreamWriter(path, false))
                {
                    sw.Write(textToSend);
                }
            }           


        }
    }
}
