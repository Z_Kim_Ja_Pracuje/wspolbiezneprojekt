﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Library;
using Newtonsoft.Json;

namespace Klient
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        TcpClient ClientSocket = new TcpClient();
        StreamWriter ServerStreamWrite { get; set; }
        StreamReader ServerStreamRead { get; set; }
        NetworkStream ClientStream { get; set; }
        Thread MainThread { get; set; }
        WindowText MainWindowText { get; set; }
        bool Logged { get; set; } = false;
        string UserName { get; set; }
        
        /// <summary>
        /// Podłączenie się do socketa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClientSocket.Connect(TextBoxIpAddress.Text, int.Parse(TextBoxPort.Text));
                ServerStreamWrite = new StreamWriter(ClientSocket.GetStream());
                ServerStreamRead = new StreamReader(ClientSocket.GetStream());
                ClientStream = ClientSocket.GetStream();
                
                StackPanelConnect.IsEnabled = false;
                StackPanelChooseLogReg.Visibility = Visibility.Visible;

                // 2018-06-07: zmiana podejścia, startujemy wątek odbierający po podłączeniu do serwera 
                // a nie po zalogowaniu
                MainThread = new Thread(ReceiveFromServer);
                MainThread.Start();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void RadioAcount_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is RadioButton rb)
            {
                if (rb.Name == "RadioHaveAcount")
                {
                    StackPanelLogin.Visibility = Visibility.Visible;
                    StackPanelRegister.Visibility = Visibility.Hidden;
                }
                else
                {
                    StackPanelLogin.Visibility = Visibility.Hidden;
                    StackPanelRegister.Visibility = Visibility.Visible;
                }
            }
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            // pola puste - coś zwrócić czy co?
            if (string.IsNullOrEmpty(TextBoxLogin.Text) || string.IsNullOrEmpty(PasswordBoxPassword.Password))
                return;

            List<byte> data = new List<byte>
            {
                (byte)ReceivedDataType.Login
            };
            data.AddRange(System.Text.Encoding.ASCII.GetBytes(TextBoxLogin.Text));
            data.Add(0);
            data.AddRange(System.Text.Encoding.ASCII.GetBytes(PasswordBoxPassword.Password));

            ServerStreamWrite.BaseStream.Write(data.ToArray(), 0, data.Count);
            ServerStreamWrite.Flush();

            UserName = TextBoxLogin.Text;

            //bool result = ReceiveLoginRegMessage();

            //if (result)
            //{
            //    // przełączenie się na drugą zakładkę
            //    TabItemStart.IsEnabled = false;
            //    TabItemCharts.IsEnabled = true;
            //    TabControl1.SelectedIndex = 1;

            //    Thread clientThread = new Thread(ReceiveFromServer);
            //    clientThread.Start();
            //    OpenSecondWindow();
            //}

        }

        private void ReceiveFromServer()
        {
            ClientSocket.ReceiveBufferSize = 250000;
            List<byte> simDataBuffer = new List<byte>();

            try {
                while (true) {
                    var stream = ClientSocket.GetStream();
                    byte[] buffer = new byte[250000];
                    int readed = stream.Read(buffer, 0, ClientSocket.Available);

                    if (readed == 0)
                        continue;

                    byte dataType = buffer[0];

                    var actualData = buffer.Skip(1).Take(readed - 1).ToArray();
                    switch (dataType) {
                        case (byte)ReceivedDataType.Login:
                        case (byte)ReceivedDataType.Register:
                            ReceiveLoginRegMessage(buffer, readed - 1);
                            break;
                        case (byte)ReceivedDataType.Message:
                            AddMessage(buffer, readed - 1);
                            break;
                        case (byte)ReceivedDataType.SimData:


                            // trzeba buforować bo nie wysyła za jednym razem
                            if (buffer[readed - 1] != 255) {
                                byte b = 0;
                                do {
                                    b = (byte)stream.ReadByte();
                                    buffer[readed++] = b;
                                }
                                while (b != 255);
                            }


                            ReceiveModelData(buffer.Skip(1).Take(readed - 2).ToArray());
                            break;

                        case (byte)ReceivedDataType.SimDataChange:
                            // odbiór zmiany modelu

                            if (buffer[readed - 1] != 255) {
                                byte b = 0;
                                do {
                                    b = (byte)stream.ReadByte();
                                    buffer[readed++] = b;
                                }
                                while (b != 255);
                            }

                            actualData = buffer.Skip(1).Take(readed - 2).ToArray();

                            ModelChanges model = JsonConvert.DeserializeObject<ModelChanges>(
                                Encoding.ASCII.GetString(actualData));

                            string modelStr = "";
                            Dispatcher.Invoke(() => {
                                modelStr = MainWindowText.ReceiveChanges(model);
                            });
                            break;
                        case (byte)ReceivedDataType.ModelRotation:
                            actualData = buffer.Skip(1).Take(readed - 1).ToArray();
                            ReceiveModelRotation(actualData);
                            break;
                    }


                    // odbiór danych
                }
            }
            catch(Exception ex) {
                Dispatcher.Invoke(() => {
                    RichTextBoxChat.AppendText(Environment.NewLine + ">> " + "Connection with serwer is ended. ");
                });
            }
        }

        private void AddMessage(byte[] buffer, int length)
        {
            Dispatcher.Invoke(() => {
                RichTextBoxChat.AppendText(Environment.NewLine + ">> " + System.Text.Encoding.ASCII.GetString(buffer.Skip(1).Take(length).ToArray()));
            });
        }

        private void Register(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxLoginReg.Text) || string.IsNullOrEmpty(PasswordBoxPasswordReg.Password)
                || string.IsNullOrEmpty(TextBoxEmailReg.Text))
                return;

            List<byte> data = new List<byte>
            {
                (byte)ReceivedDataType.Register
            };
            data.AddRange(System.Text.Encoding.ASCII.GetBytes(TextBoxLoginReg.Text));
            data.Add(0);
            data.AddRange(System.Text.Encoding.ASCII.GetBytes(PasswordBoxPasswordReg.Password));
            data.Add(0);
            data.AddRange(System.Text.Encoding.ASCII.GetBytes(TextBoxEmailReg.Text));

            ServerStreamWrite.BaseStream.Write(data.ToArray(), 0, data.Count);
            ServerStreamWrite.Flush();

            UserName = TextBoxLoginReg.Text;

            //  rejestracja - skopiować login

            // 

            //bool result = ReceiveLoginRegMessage();

            //if (result)
            //{
            //    TabItemStart.IsEnabled = false;
            //    TabItemCharts.IsEnabled = true;
            //    TabControl1.SelectedIndex = 1;

            //    Thread clientThread = new Thread(ReceiveFromServer);
            //    clientThread.Start();
            //}
        }

        public bool ReceiveLoginRegMessage(byte[] buffer, int length)
        {
            bool result = false;
            var temp = buffer.Skip(1).Take(length).ToArray();

            string response = Encoding.ASCII.GetString(temp);

            if (response.StartsWith("You have been"))
                result = true;
            else // błąd
                result = false;

            this.Dispatcher.Invoke(() => {
                TextBlockStatus.Text = response;
                if (result)
                    TextBlockStatus.Foreground = Brushes.Green;
                else
                    TextBlockStatus.Foreground = Brushes.Green;
            }, System.Windows.Threading.DispatcherPriority.Normal);

            Task.Run(async () => {
                await Task.Delay(10000);
                Dispatcher.Invoke(() =>
                    TextBlockStatus.Text = "");
            });

            // obsługa 
            if (result)
            {
                Dispatcher.Invoke(() => {
                    TabItemStart.IsEnabled = false;
                    TabItemCharts.IsEnabled = true;
                    TabControl1.SelectedIndex = 1;
                });
                Logged = true;
                OpenSecondWindow();
            }

            return result;
        }

        private void SendMessage(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TextBoxInput.Text))
            {
                List<byte> message = Encoding.ASCII.GetBytes(TextBoxInput.Text + "$").ToList();
                message.Insert(0, (byte)ReceivedDataType.Message);
                ClientStream.Write(message.ToArray(), 0, message.Count);
            }
        }

        private void OpenSecondWindow()
        {
            Dispatcher.Invoke(() => {
                MainWindowText = new WindowText();
                MainWindowText.Init(SendChanges);
                MainWindowText.Show();
            });
        }

        private void ReceiveModelData(byte[] buffer)
        {
            // wpisanie do okienka
            string result = Encoding.ASCII.GetString(buffer);

            string[] splitted = result.Split('#');
            if(splitted.Length == 1) {
                // error - pasuje ustawić jakiś label, czy coś
                Dispatcher.Invoke(() => {
                    MainWindowText.SetLabelInfo(result);
                });
                return;
            }

            Dispatcher.Invoke(() => {
                MainWindowText.ReceiveModel(splitted);
            });

            ModelRepresentation mr = (splitted.Length == 2) ? new ModelRepresentation(splitted[1]) : new ModelRepresentation(splitted[2]);

            // i wyrenderowanie 
            RenderModelData(mr);
        }

        public void AskForModelData(string number)
        {
            if (ClientSocket != null && !string.IsNullOrEmpty(number))
            {
                List<byte> request = new List<byte>();
                request.Add((byte)ReceivedDataType.SimData);
                request.AddRange(Encoding.ASCII.GetBytes(number));

                ClientSocket.GetStream().Write(request.ToArray(), 0, request.Count());
                ClientSocket.GetStream().Flush();
            }

        }

        private void SendChanges(ModelChanges mc)
        {
            mc.UserName = UserName;
            string json = JsonConvert.SerializeObject(mc);
            List<byte> buffer = Encoding.ASCII.GetBytes(json).ToList();
            buffer.Insert(0, (byte)ReceivedDataType.SimDataChange);
            buffer.Add((byte)ReceivedDataType.EndByte);

            ClientStream.Write(buffer.ToArray(), 0, buffer.Count);
            ClientStream.Flush();
        }

        private void RenderModelData(ModelRepresentation mr)
        {
            // renderowanie - na podstawie obiektu ModelRepresentation


            // temperatura - spektrum barw, wyrysować takie gówno obok i walnąć jakąs legendę (min, max i 5 wartości pośrednich)

            var max = Math.Round(mr.Nodes.Max(x => x.Temp), 2);
            var min = Math.Round(mr.Nodes.Min(x => x.Temp), 2);

            double space = (max - min) / 6.0d; // do wyrysowania legendy
            space = Math.Round(space, 2);

            var l = new LinearGradientBrush(new GradientStopCollection() {
                new GradientStop(Colors.Blue, 0.0),
                new GradientStop(Colors.Cyan, 0.25),
                new GradientStop(Colors.Lime, 0.5),
                new GradientStop(Colors.Yellow, 0.75),
                new GradientStop(Colors.Red, 1)
            });

            // jeszcze jakoś uzupełnić label z legendą - tip: justowanie

            l.Freeze();

            Dispatcher.Invoke(() => {
                var values = new List<double>() {
                    min + space,
                    min + 2*space,
                    min + 3*space,
                    min + 4*space,
                    min + 5*space
                };

                var temp = string.Join("\t   ", values);

                TextBlockLegend.Text = $"{min}\t   {temp}\t   {max}";
                TextBlockLegend.Background = l;
                // TextBlockLegend.TextAlignment = TextAlignment.Justify;
            });

            //  tylko na pokaz
            

            this.Dispatcher.Invoke(() => {
               
                // RectangleColorSpectrum.Fill = l;
                RectangleColorSpectrum.UpdateLayout();
            });

            var firstNode = mr.Nodes.Where(x => x.X + x.Y + x.Z == mr.Nodes.Min(y => y.X + y.Y + y.Z)).First();
            var lastNode = mr.Nodes.Where(x => x.X + x.Y + x.Z == mr.Nodes.Max(y => y.X + y.Y + y.Z));
            
            this.Dispatcher.Invoke(() => {
                this.VisualEdgeMode = EdgeMode.Aliased;
                List<Color> colorMap = new List<Color>() {
                    Colors.Blue,
                    Colors.Cyan,
                    Colors.Lime,
                    Colors.Yellow,
                    Colors.Red
                };

                foreach (var node in mr.Elements) {
                    ModelVisual3D modelVisual3D = new ModelVisual3D();

                    var nn = node.Select(x => mr.Nodes[x - 1]).Average(x=>x.Temp);
                    var color = GetColorForValue(nn, min, max, colorMap);

                    // modelVisual3D.Content = CreateCubeModel(mr, node, color);
                     modelVisual3D.Content = CreateTestModel(node, mr.Nodes, color);
                   
                    Viewport3DModel.Children.Add(modelVisual3D);
                    
                }
                // Viewport3DModel.Children.First().

                // czerwone światło, nie wiem dlaczego ale działa
                ModelVisual3D mv3 = new ModelVisual3D();
                mv3.Content = new AmbientLight(Colors.Red);
                Viewport3DModel.Children.Add(mv3);
                // Viewport3DModel.V
            });

        }

        // wersja nieoptymalna - działa jak serwery ubisoftu
        private Model3DGroup CreateCubeModel(ModelRepresentation mr, int[] nodes, Color c) {
            Model3DGroup cube = new Model3DGroup();

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[2] - 1], mr.Nodes[nodes[0] - 1], mr.Nodes[nodes[1] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[0] - 1], mr.Nodes[nodes[3] - 1], mr.Nodes[nodes[2] - 1], c));

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[1] - 1], mr.Nodes[nodes[0] - 1], mr.Nodes[nodes[4] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[1] - 1], mr.Nodes[nodes[4] - 1], mr.Nodes[nodes[5] - 1], c));

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[1] - 1], mr.Nodes[nodes[2] - 1], mr.Nodes[nodes[5] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[5] - 1], mr.Nodes[nodes[2] - 1], mr.Nodes[nodes[6] - 1], c));

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[2] - 1], mr.Nodes[nodes[3] - 1], mr.Nodes[nodes[6] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[3] - 1], mr.Nodes[nodes[7] - 1], mr.Nodes[nodes[6] - 1], c));

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[0] - 1], mr.Nodes[nodes[3] - 1], mr.Nodes[nodes[7] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[0] - 1], mr.Nodes[nodes[4] - 1], mr.Nodes[nodes[7] - 1], c));

            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[4] - 1], mr.Nodes[nodes[5] - 1], mr.Nodes[nodes[6] - 1], c));
            cube.Children.Add(CreateTriangleModel(mr.Nodes[nodes[4] - 1], mr.Nodes[nodes[6] - 1], mr.Nodes[nodes[7] - 1], c));

            return cube;
        }

        // wersja optymalna - działa sporo szybciej
        private Model3DGroup CreateTestModel(int[] arr, List<Node> list, Color c) {
            MeshGeometry3D mesh = new MeshGeometry3D();
            var nodes = arr.Select(x => list[x - 1]).ToList();
            foreach(var n in nodes) {
                mesh.Positions.Add(new Point3D(n.X, n.Y, n.Z));
            }

            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);

            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(2);

            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(4);

            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(5);

            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(5);

            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(6);

            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(6);

            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(6);

            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(7);

            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(7);

            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(6);

            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(7);



            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(2);

            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(0);

            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);

            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(1);

            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(1);

            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(5);

            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(2);

            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(3);

            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(3);
            mesh.TriangleIndices.Add(0);

            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(4);
            mesh.TriangleIndices.Add(0);

            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(5);
            mesh.TriangleIndices.Add(4);

            mesh.TriangleIndices.Add(7);
            mesh.TriangleIndices.Add(6);
            mesh.TriangleIndices.Add(4);


            Material material = new EmissiveMaterial(
                new SolidColorBrush(c));

            Material m2 = new DiffuseMaterial(
                new SolidColorBrush(c));

            MaterialGroup mg = new MaterialGroup();
            mg.Children.Add(m2);
            mg.Children.Add(material);

            GeometryModel3D model = new GeometryModel3D(
                mesh, mg);

            Model3DGroup group = new Model3DGroup();

            group.Children.Add(model);
            return group;
        }

        private Model3DGroup CreateTriangleModel(Node n0, Node n1, Node n2, Color c) {
            
            MeshGeometry3D mesh = new MeshGeometry3D();
            Point3D p0 = new Point3D(n0.X, n0.Y, n0.Z);
            Point3D p1 = new Point3D(n1.X, n1.Y, n1.Z);
            Point3D p2 = new Point3D(n2.X, n2.Y, n2.Z);

            mesh.Positions.Add(p0);
            mesh.Positions.Add(p1);
            mesh.Positions.Add(p2);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);

            mesh.TriangleIndices.Add(2);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(0);
            
            Material material = new EmissiveMaterial(
                new SolidColorBrush(c));
            
            Material m2 = new DiffuseMaterial(
                new SolidColorBrush(c));

            MaterialGroup mg = new MaterialGroup();
            mg.Children.Add(m2);
            mg.Children.Add(material);
            
            GeometryModel3D model = new GeometryModel3D(
                mesh, mg);

            Model3DGroup group = new Model3DGroup();
            
            group.Children.Add(model);
            return group;
        }

        private Color GetColorForValue(double val, double minVal, double maxVal, List<Color> ColorsOfMap) {
            double valPerc = (val - minVal) / (maxVal - minVal);// value%
            double colorPerc = 1d / (ColorsOfMap.Count - 1);// % of each block of color. the last is the "100% Color"
            double blockOfColor = valPerc / colorPerc;// the integer part repersents how many block to skip
            int blockIdx = (int)Math.Truncate(blockOfColor);// Idx of 
            double valPercResidual = valPerc - (blockIdx * colorPerc);//remove the part represented of block 
            double percOfColor = valPercResidual / colorPerc;// % of color of this block that will be filled

            Color cTarget = ColorsOfMap[blockIdx];
            Color cNext = cNext = ColorsOfMap[blockIdx + 1];

            var deltaR = cNext.R - cTarget.R;
            var deltaG = cNext.G - cTarget.G;
            var deltaB = cNext.B - cTarget.B;

            var R = cTarget.R + (deltaR * percOfColor);
            var G = cTarget.G + (deltaG * percOfColor);
            var B = cTarget.B + (deltaB * percOfColor);

            Color c = ColorsOfMap[0];
            try {
                c = Color.FromArgb(0xff, (byte)R, (byte)G, (byte)B);
            }
            catch (Exception ex) {
            }
            return c;
        }

        private void GetModelData(object sender, RoutedEventArgs e)
        {
            AskForModelData(TextBoxFileNumber.Text);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(MainThread != null && MainThread.IsAlive)
            {
                try
                {
                    MainThread.Abort();
                }
                finally { /*olewamy wyjątek*/ }
            }
        }

        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {

            Transform3DGroup t3dg = new Transform3DGroup();
            t3dg.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), Slider1.Value)));
            t3dg.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), Slider2.Value)));

            Viewport3DModel.Camera.Transform = t3dg;

            // + przesyłanie rotacji na serwer + odbiór rotacji na serwerze + broadcast
            // pasuje napisać jakąś uniwersalną metodę do broadcastu


        }

        private void Slider2_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            List<byte> rotation = new List<byte>();
            rotation.Add((byte)ReceivedDataType.ModelRotation);
            rotation.AddRange(Encoding.ASCII.GetBytes($"{Slider1.Value};{Slider2.Value}"));

            ClientStream.Write(rotation.ToArray(), 0, rotation.Count);
         }

        private void ReceiveModelRotation(byte[] buffer) {
            Dispatcher.Invoke(() => {
                var arr = Encoding.ASCII.GetString(buffer).Split(';');
                if (arr.Length == 2) {
                    Slider1.Value = double.Parse(arr[0]);
                    Slider2.Value = double.Parse(arr[1]);
                }
            });
            
            
        }

        private void Slider2_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e) {
            List<byte> rotation = new List<byte>();
            rotation.Add((byte)ReceivedDataType.ModelRotation);
            rotation.AddRange(Encoding.ASCII.GetBytes($"{Slider1.Value};{Slider2.Value}"));

            ClientStream.Write(rotation.ToArray(), 0, rotation.Count);
        }
        
    }
}
