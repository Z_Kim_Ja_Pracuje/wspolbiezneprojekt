﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library {
    public class ModelChanges {
        public Dictionary<int, string> Changes { get; set; }
        public string Filename { get; set; }
        public string UserName { get; set; }
    }
}
