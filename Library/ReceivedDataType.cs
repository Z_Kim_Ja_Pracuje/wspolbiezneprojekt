﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Typ otrzymywanych danych - czyli czy rejestracja, czy logowanie etc.
    /// </summary>
    public enum ReceivedDataType : byte
    {
        EndByte = 255,
        Register = 0,
        Login = 1,
        LogOut = 2,
        Message = 3,
        SimData = 4,
        ModelRotation = 5,
        SimDataChange = 6,
        PdfReport = 7,

    }
    
}
