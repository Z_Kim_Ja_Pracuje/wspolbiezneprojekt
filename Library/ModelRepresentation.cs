﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library
{
    public class ModelRepresentation
    {
        public List<Node> Nodes = new List<Node>();
        public List<int[]> Elements = new List<int[]>();

        public ModelRepresentation() { }
        public ModelRepresentation(string model)
        {
            model = Regex.Replace(model, @"[ \t\r]+", "");
            var splitted = model.Split(new string[] { "\n" }, StringSplitOptions.None);
            int endPosition = 0;
            for(int i = 0; i < splitted.Length; i++) {
                string s = splitted[i];

                if (s == "*NODE") continue;

                var s1 = s.Split(',');

                if (s1.Length < 5 && s1[0] != "*NODE") {
                    endPosition = i;
                    break;
                }
                Nodes.Add(new Node(s1));
            }

            for(int i = endPosition + 1; i < splitted.Length; i++) {
                if (splitted[i] == "*END")
                    break;

                var s1 = splitted[i].Split(',');
                List<int> cube = new List<int>();

                // dlaczego od 2? bo s1[0] to ID a s1[1] to chyba numer siatki
                for(int j = 2; j < s1.Length; j++) {
                    cube.Add(int.Parse(s1[j]));
                }
                Elements.Add(cube.ToArray());
            }
        }
    }

    public class Node
    {
        public int ID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double Temp { get; set; }

        public Node() { }
        public Node(string[] values)
        {
            ID = int.Parse(values[0]);
            X = double.Parse(values[1], CultureInfo.InvariantCulture);
            Y = double.Parse(values[2], CultureInfo.InvariantCulture);
            Z = double.Parse(values[3], CultureInfo.InvariantCulture);
            Temp = double.Parse(values[4], CultureInfo.InvariantCulture);
        }
    }
}
