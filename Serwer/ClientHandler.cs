﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using Serwer.Model;
using System.Configuration;
using LiteDB;
using Library;
using System.Collections.Generic;
using log4net;
using System.Net;
using Newtonsoft.Json;

namespace Serwer
{

    class ClientHandler
    {
        TcpClient clientSocket;
        string clientNumber;
        Hashtable clientsList;
        readonly Form1 form;
        Thread clientThread;
        private readonly string databaseName;
        private readonly string databaseNameKey = "DatabaseName";
        private readonly ILog log;
        private string clientLogin;
        private string Ip = "";

        private static volatile object locker;

        static ClientHandler() {
            locker = new object();
        }

        public ClientHandler(Form1 _form)
        {
            this.form = _form;
            databaseName = ConfigurationManager.AppSettings[databaseNameKey];
            log = Form1.log;
        }

        public void startClient(TcpClient _inClientSocket, string _clientNumber, Hashtable _clientsList)
        {
            this.clientSocket = _inClientSocket;
            this.clientNumber = _clientNumber;
            this.clientsList = _clientsList;            
            clientThread = new Thread(ReceiveData);
            clientThread.Start();
        }

        private byte[] GetBytes(string str) {
            return Encoding.ASCII.GetBytes(str);
        }

        private byte[] GetBytes(string str, ReceivedDataType type) {
            List<byte> bytes = GetBytes(str).ToList();
            bytes.Insert(0, (byte)type);
            return bytes.ToArray();
        }

        private byte[] GetBytes(string str, byte type) {
            return GetBytes(str, (ReceivedDataType)type);
        }
        

        private void ReceiveData()
        {
            try {
                int requestCount = 0;
                byte[] bytesFrom = new byte[10025];
                string dataFromClient = null;
                Byte[] sendBytes = null;
                string serverResponse = null;
                string rCount = null;
                requestCount = 0;
                int errorCounter = 0;
                Ip = ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString();

                StreamWriter sw = new StreamWriter(clientSocket.GetStream());
                NetworkStream networkStream = clientSocket.GetStream();

                while (true) {
                    try {
                        requestCount = requestCount + 1;
                        int readed = networkStream.Read(bytesFrom, 0, clientSocket.Available);

                        if (readed == 0) // zdarza się coś takiego
                            continue;

                        var firstByte = bytesFrom[0];
                        var tmp = bytesFrom.Skip(1).Take(readed - 1).ToArray();

                        switch (firstByte) {
                            case (byte)ReceivedDataType.Register:
                                // rejestracja - czyli poleci login, hasło i email
                                // wysłanie response
                                try {
                                    serverResponse = TryRegister(tmp);
                                    if (!string.IsNullOrEmpty(clientLogin)) {
                                        form.saveSessionLog($"{clientLogin} ({Ip}) logged in");
                                    }
                                }
                                catch (Exception ex) {
                                    serverResponse = "An exception occured while registering. Try again later or contact administrator.";
                                    log.ErrorFormat("Register exception from IP: {0} - {1}", ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString(), ex);
                                }
                                byte[] regResponse = GetBytes(serverResponse, firstByte);
                                networkStream.Write(regResponse, 0, regResponse.Length);
                                networkStream.Flush();
                                break;

                            case (byte)ReceivedDataType.Login:
                                // logowanie - czyli wyszukanie danych w bazie, dodanie socketa do listy
                                // wysłanie response
                                try {
                                    serverResponse = TryLogin(tmp);
                                    if (!string.IsNullOrEmpty(clientLogin)) {
                                        form.saveSessionLog($"{clientLogin} ({Ip}) logged in");
                                    }
                                }
                                catch (Exception ex) {
                                    serverResponse = "An exception occured while logging. Try again later or contact administrator.";
                                    log.ErrorFormat("Login exception from IP: {0} - {1}", ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString(), ex);
                                }

                                byte[] logResponse = GetBytes(serverResponse, firstByte);
                                networkStream.Write(logResponse, 0, logResponse.Length);
                                networkStream.Flush();

                                break;

                            case (byte)ReceivedDataType.LogOut:
                                // wylogowanie

                                serverResponse = Logout(tmp);

                                byte[] logoutResponse = GetBytes(serverResponse, firstByte);
                                networkStream.Write(logoutResponse, 0, logoutResponse.Length);
                                networkStream.Flush();

                                break;

                            case (byte)ReceivedDataType.Message:
                                // wiadomość - czyli odczyt i broadcast
                                dataFromClient = System.Text.Encoding.ASCII.GetString(tmp);
                                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));

                                Form1.Broadcast(dataFromClient, clientLogin, true);
                                form.saveSessionLog("Message from  " + clientLogin + ": " + dataFromClient);
                                rCount = Convert.ToString(requestCount);
                                break;

                            case (byte)ReceivedDataType.SimData:
                                // wysyłanie przez serwer danych z pliku
                                string number = Encoding.ASCII.GetString(tmp);
                                form.saveSessionLog($"-- User {clientLogin} ({Ip}) requested for file {number}");
                                string result = GetAndReturnModel(number);
                                List<byte> response = new List<byte>(Encoding.ASCII.GetBytes(result));
                                response.Insert(0, (byte)ReceivedDataType.SimData);
                                response.Add(255);

                                networkStream.Write(response.ToArray(), 0, response.Count);
                                networkStream.Flush();
                                break;

                            case (byte)ReceivedDataType.SimDataChange:
                                // trzeba:
                                // 1.Rozesłać do wszystkich - jakiś broadcast, czy cuś
                                // 2. Update pliku - i tu jest pewien problem - mianowicie
                                // pasuje zrobić repo, tylko jak dostawać się do starych wersji?
                                // Zamiast wybierać plik z palca to po prostu wysyłać do klienta listę tych plików, wraz z wersjami i
                                // kto je modyfikował? Tylko, że to duży narzut na sieć. I po każdym zapisie trzeba robić update listy.

                                if(bytesFrom[readed-1] != (byte)ReceivedDataType.EndByte) {
                                    byte b = 0;
                                    do {
                                        b = (byte)networkStream.ReadByte();
                                        bytesFrom[readed++] = b;
                                    }
                                    while (b != 255);
                                }

                                string json = Encoding.ASCII.GetString(bytesFrom.Skip(1).Take(readed - 2).ToArray());

                                var mc = JsonConvert.DeserializeObject<ModelChanges>(json);

                                BroadcastChanges(bytesFrom, readed, mc);
                                LogChanges(mc);

                                break;
                            case (byte)ReceivedDataType.ModelRotation:
                                Broadcast(bytesFrom, readed - 1);
                                break;
                        }
                        errorCounter = 0;
                        // Form1.broadcast(dataFromClient, clientLogin, true);
                    }
                    catch (ObjectDisposedException odex) {
                        // w tym wypadku nie ma już socketa - proponuję wyjść z pętli
                        Console.WriteLine(odex);
                        log.ErrorFormat("Socket disposed: {0}", odex);
                        break;
                    }
                    catch (Exception ex) {
                        errorCounter++;
                        Console.WriteLine(ex.ToString());
                        log.ErrorFormat("Error occured for IP {0} - {1}", ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString(), ex);
                    }
                    if (errorCounter > 10) {
                        // coś się podziało, najprawdopodobniej padł socket

                        log.WarnFormat("ErrorCounter for IP {0} reached value {1} - closing connection.",
                            ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString(), errorCounter);
                        clientSocket.Close();
                        break;
                    }
                }

                form.saveSessionLog($"{clientLogin} ({Ip}) has left.");
            }
            catch(Exception ex) {
                log.ErrorFormat("Exception: {0}", ex);
                form.saveSessionLog($"Connection broken with user {clientLogin} ({Ip}).");
            }
        }

        private string TryRegister(byte[] data) {
            string[] splitted = new string[3];
            int position = 0;
            int lastIndex = 0;
            for(int i = 0; i < data.Length; i++) {
                if(data[i] == 0) {
                    splitted[position++] = System.Text.Encoding.ASCII.GetString(data.ToList().GetRange(lastIndex, i - lastIndex ).ToArray());
                    lastIndex = i + 1;
                }
            }
            splitted[position] = System.Text.Encoding.ASCII.GetString(data.ToList().GetRange(lastIndex, data.Length - lastIndex).ToArray());

            User user = new User();
            if (!user.FillData(splitted)) {
                return "An error occured during registration - check, if inserted data is valid and try again later";
            }

            using (var db = new LiteDatabase(databaseName)) {
                var users = db.GetCollection<User>("User");

                var result = users.Find(x => x.Username == user.Username || x.Email == user.Email);
                if (result.Any()) {
                    return "Username and/or email is taken.";
                }

                users.Insert(user);
            }
            Login(user);

            // wszystko ok
            return "You have been succesfully registered.";
        }

        private string Login(User user) {
            using (var db = new LiteDatabase(databaseName)) {
                var users = db.GetCollection<User>("User");
                var result = users.Find(x => x.Username == user.Username && x.Password == user.Password);
                if (!result.Any()) {
                    return "Username and/or password is wrong.";
                }

                lock (clientsList.SyncRoot) {
                    clientsList.Add(user.Username, clientSocket);
                }
                clientLogin = user.Username;
                return "You have been succesfully logged.";
            }
        }

        private string TryLogin(byte[] data) {
            int index = data.ToList().IndexOf(0);
            if(index == -1) {
                return "An error occured during logging - check, if inserted data is valid and try again later. ";
            }
            User user = new User();
            user.Username = System.Text.Encoding.ASCII.GetString(data, 0, index);
            user.Password = System.Text.Encoding.ASCII.GetString(data, index + 1, data.Length - index - 1 );
            return Login(user);
        }

        private string Logout(byte[] data) {
            string username = Encoding.ASCII.GetString(data);
            lock (clientsList.SyncRoot) {
                var client = clientsList[username];
                if(client == null) {
                    return "You can not logout - you are not logged in.";
                }
                clientsList.Remove(username);
                return "Logout successful.";
            }
        }

        private string GetAndReturnModel(string number)
        {
            try
            {
                // tutaj trzeba będzie pozmieniać - jeśli ma być repozytorium plików
                // to w pierwszej kolejności trzeba sprawdzić, czy jest odniesienie w bazie
                // Model.ResultsRepository - jeśli jest to pobieramy po nazwie pliku z bazy i elo
                string fileName = $"temperature {new string('0', 5 - number.Length)}{number}.k";
                string FileName = $"../../Zbior_testowy/{fileName}";
                string info = "";
                using (var db = new LiteDatabase(databaseName)) {
                    var files = db.GetCollection<ResultsRepository>();

                    // podmiana ścieżki i stosowny komunikat - pomiędzy nazwą pliku i treścią
                    // zapis do bazy jak i zapis pliku - przy EndSession - trzeba przerobić

                    var f = files.Find(x => x.FileName == fileName);
                    if (f.Any()) {
                        f.OrderBy(x => x.LastModified);
                        var searchedFile = f.First();

                        int fileCount = f.Count() + 1;

                        FileName = $"{Form1.PathToRepo}{searchedFile.ActualFileName}";
                        string date = searchedFile.LastModified.ToString("d.M.yyyy, HH:mm:ss");
                        info = $"Server find {fileCount} files named {fileName}." +
                            $"\nYou received newest version from {date}. Changes list: \n";

                        string OldLines = "Old lines:\n";
                        foreach(var i in searchedFile.OldLines) {
                            OldLines += $"Line {i.Key}: {i.Value}\n";
                        }
                        string NewLines = "New lines:\n";
                        foreach(var i in searchedFile.NewLines) {
                            NewLines += $"Line {i.Key}: {i.Value}\n";
                        }

                        info += $"{NewLines}\n{OldLines}";
                        
                    }
                    
                }

                // string fileName = $"temperature {new string('0', 5 - number.Length)}{number}.k";

                // test
                string result = "";
                if (Form1.ModelData.TryGetValue(fileName, out string res)) {
                    result = Form1.ModelData[fileName];
                }
                else {
                    
                    var stream = File.OpenText(FileName);

                    result = stream.ReadToEnd();

                    lock (locker) {
                        Form1.ModelData.Add(fileName, result);
                    }
                }

                // jeśli plik z repo to stosowny komunikat
                if (!string.IsNullOrEmpty(info))
                    return $"{fileName}#{info}#{result}";
                else
                    return $"{fileName}#{result}";
            }
            catch(Exception ex)
            {
                log.ErrorFormat("Exception occured while opening file number '{0}' - {1}", number, ex);
                return ex.Message;

            }
        }

        private void BroadcastChanges(byte[] tmp, int readed, ModelChanges mc) {
            
            lock (clientsList.SyncRoot) {
                foreach(DictionaryEntry de in clientsList) {
                    if(mc.UserName != de.Key.ToString()) { // test
                        var client = de.Value as TcpClient;
                        var stream = client.GetStream();
                        stream.Write(tmp, 0, readed);
                    }
                }
            }
        }

        private void Broadcast(byte[] request, int readed) {
            byte[] arr = request.Skip(1).Take(readed).ToArray();
            var splitted = Encoding.ASCII.GetString(arr).Split(';');

            form.saveSessionLog($"User {clientLogin} ({Ip}) rotated model by {splitted[0]} and {splitted[1]} degrees.");
            lock (clientsList.SyncRoot) {
                foreach (DictionaryEntry de in clientsList) {
                    if ((de.Key as string) == clientLogin) continue;
                    var client = de.Value as TcpClient;
                    var stream = client.GetStream();
                    stream.Write(request, 0, readed+1);
                }
            }

        }

        private void LogChanges(ModelChanges mc) {

            // pierwsze co - wprowadź zmiany do modelu
            lock (locker) {
                if(Form1.ModelData.TryGetValue(mc.Filename, out string value)) {
                    var splited = value.Split('\n');
                    foreach (var i in mc.Changes)
                        splited[i.Key] = i.Value;
                    value = splited.Aggregate((curr, next) => $"{curr}\n{next}");
                    Form1.ModelData[mc.Filename] = value;
                    if (!Form1.ChangedFiles.Contains(mc.Filename))
                        Form1.ChangedFiles.Add(mc.Filename);
                }

            }

            string changesList = string.Join("\n", mc.Changes.Select(x => $"Line {x.Key} : {x.Value}"));

            form.saveSessionLog($"User {clientLogin} ({Ip}) sended changes of file {mc.Filename}:" +
                $"\n{changesList}");
        }

        public void StopThread() {
            if (clientThread != null)
                clientThread.Abort();
        }
    }
}
