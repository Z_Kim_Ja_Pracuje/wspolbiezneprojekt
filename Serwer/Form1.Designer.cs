﻿namespace Serwer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.startSessionButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.chatConsoleTextBox = new System.Windows.Forms.RichTextBox();
            this.saveToPDFButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(12, 27);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(100, 20);
            this.portTextBox.TabIndex = 1;
            this.portTextBox.Text = "3000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Port";
            // 
            // startSessionButton
            // 
            this.startSessionButton.Location = new System.Drawing.Point(12, 53);
            this.startSessionButton.Name = "startSessionButton";
            this.startSessionButton.Size = new System.Drawing.Size(100, 23);
            this.startSessionButton.TabIndex = 4;
            this.startSessionButton.Text = "Start Session";
            this.startSessionButton.UseVisualStyleBackColor = true;
            this.startSessionButton.Click += new System.EventHandler(this.startSessionButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 82);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Stop Session";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.StopSession_Click);
            // 
            // chatConsoleTextBox
            // 
            this.chatConsoleTextBox.Location = new System.Drawing.Point(118, 27);
            this.chatConsoleTextBox.Name = "chatConsoleTextBox";
            this.chatConsoleTextBox.Size = new System.Drawing.Size(405, 479);
            this.chatConsoleTextBox.TabIndex = 6;
            this.chatConsoleTextBox.Text = "";
            // 
            // saveToPDFButton
            // 
            this.saveToPDFButton.Location = new System.Drawing.Point(12, 111);
            this.saveToPDFButton.Name = "saveToPDFButton";
            this.saveToPDFButton.Size = new System.Drawing.Size(100, 23);
            this.saveToPDFButton.TabIndex = 7;
            this.saveToPDFButton.Text = "Save To PDF";
            this.saveToPDFButton.UseVisualStyleBackColor = true;
            this.saveToPDFButton.Click += new System.EventHandler(this.saveToPDFButton_Click);
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(537, 542);
            this.Controls.Add(this.saveToPDFButton);
            this.Controls.Add(this.chatConsoleTextBox);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.startSessionButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.portTextBox);
            this.Name = "Form1";
            this.Text = "Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button startSessionButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.RichTextBox chatConsoleTextBox;
        private System.Windows.Forms.Button saveToPDFButton;
    }
}

