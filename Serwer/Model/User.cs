﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serwer.Model {
    public class User {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public User() { }
        
        public bool FillData(string[] data) {
            if(data.Length < 3 || data.Length > 3) {
                return false;
            }

            Username = data[0];
            Password = data[1]; // potem można przemyśleć hashowanie
            Email = data[2];
            return true;
        }
    }
}
