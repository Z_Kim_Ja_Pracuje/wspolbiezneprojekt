﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serwer.Model {
    public class ResultsRepository {
        public int Id { get; set; }
        public DateTime LastModified { get; set; }
        public int WhoModified { get; set; }

        // nazwa pliku zgodna z tym formatem temperature 4543.k
        public string FileName { get; set; }

        // aktualna nazwa pliku - czyli temperature345.k{jakiś ciąg znaków}
        public string ActualFileName { get; set; }

        // zmienione dane
        public Dictionary<int, string> NewLines { get; set; }
        public Dictionary<int, string> OldLines { get; set; }
    }
}
