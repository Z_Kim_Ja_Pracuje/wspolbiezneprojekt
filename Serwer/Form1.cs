﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using log4net;
using LiteDB;
using Serwer.Model;
using System.Configuration;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace Serwer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            databaseName = ConfigurationManager.AppSettings[databaseNameKey];
        }

        public static Hashtable clientsList = new Hashtable();
        private List<ClientHandler> clientHandlers = new List<ClientHandler>();
        Thread searchingForClientsThread = null;
        public static Dictionary<string, string> ModelData = new Dictionary<string, string>();
        public static List<string> ChangedFiles = new List<string>();
        private TcpListener ServerSocket { get; set; }
        public static readonly ILog log = LogManager.GetLogger(typeof(Form1));
        public static readonly string PathToRepo = "../../Repo/";

        private readonly string databaseName;
        private readonly string databaseNameKey = "DatabaseName";

        public string ChatTextBox
        {
            get
            {
                return chatConsoleTextBox.Text;
            }
            set
            {
                chatConsoleTextBox.Text = value;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void startSessionButton_Click(object sender, EventArgs e)
        {
            chatConsoleTextBox.Text = chatConsoleTextBox.Text + Environment.NewLine + ">> Creating new session...";
            searchingForClientsThread = new Thread(startSessionThread);
            searchingForClientsThread.Start();

        }

        private void startSessionThread()
        {
            int counter = 0;
            int portNumber = int.Parse(portTextBox.Text);
            string localIP = null;

            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }

            IPAddress localAddress = IPAddress.Parse(localIP);
            TcpListener serverSocket = new TcpListener(localAddress, portNumber);
            ServerSocket = serverSocket;
            TcpClient clientSocket = default(TcpClient);

            using (var db = new LiteDatabase(databaseName)) {
                var users = db.GetCollection<User>("User");
                int count = users.Count();

                var result = users.FindAll();
                foreach(var u in result) {
                    log.Info($"{u.Id}, {u.Username}, {u.Password}, {u.Email}");
                }

            }

            StartSession(counter, serverSocket, clientSocket);
        }


        private void StartSession(int counter, TcpListener serverSocket, TcpClient clientSocket)
        {
            serverSocket.Start();
            saveSessionLog("New session created...");
            //bool canSearchforClient = true;
            counter = 0;


            // tu trzeba trochę pozmieniać - pasuje umożliwić logowanie/rejestrację
            try {
                while (true) {
                    counter += 1;
                    clientSocket = serverSocket.AcceptTcpClient();

                    var ip = ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address.ToString();

                    saveSessionLog(ip + " joined");

                    ClientHandler client = new ClientHandler(this);
                    clientHandlers.Add(client);
                    client.startClient(clientSocket, null, clientsList);
                }
            }
            catch(Exception ex) {
                log.Error(ex);
            }

            clientSocket.Close();
            serverSocket.Stop();
            saveSessionLog("Session ended");
            Console.ReadLine();
        }

        public static void Broadcast(string message, string uName, bool flag)
        {
            foreach(DictionaryEntry Item in clientsList)
            {
                TcpClient broadcastSocket = Item.Value as TcpClient;
                NetworkStream broadcastStream = broadcastSocket.GetStream();
                message = (flag) ? uName + ": " + message : message;
                var response = Encoding.ASCII.GetBytes(message).ToList();
                response.Insert(0, (byte)Library.ReceivedDataType.Message);

                broadcastStream.Write(response.ToArray(), 0, response.Count);
                broadcastStream.Flush();
                //using (StreamWriter sw = new StreamWriter(broadcastSocket.GetStream())) {
                //    message = (flag) ? uName + ": " + message : message;
                //    sw.Write(message);
                //    sw.Flush();
                //}
            }
        }

        public void saveSessionLog(string data)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string>(saveSessionLog), new object[] {data });
            }
            else
            {
                chatConsoleTextBox.Text = chatConsoleTextBox.Text + Environment.NewLine + ">> " + data;
                
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            if (searchingForClientsThread != null)
                searchingForClientsThread.Abort();

            foreach (var cH in clientHandlers)
                cH.StopThread();
        }

        private void saveToPDFButton_Click(object sender, EventArgs e)
        {
            int numberOfLines = chatConsoleTextBox.Lines.Length;
            int numberOfPages = 1 + (numberOfLines / 40);
            

            PdfPage[] pdfPages = new PdfPage[numberOfPages];

            PdfDocument pdfDocument = new PdfDocument();
            
            for(int i = 0; i < numberOfPages; i++)
            {
                pdfPages[i] = pdfDocument.AddPage();
            }

            int fontSize = 12;

            XGraphics graph = XGraphics.FromPdfPage(pdfPages[0]);
            XFont font = new XFont("Verdana", fontSize, XFontStyle.Bold);


            string date = DateTime.Now.ToString();

            graph.DrawString("Document created at " + date, font, XBrushes.Black,
            new XRect(0, 0, pdfPages[0].Width.Point, pdfPages[0].Height.Point), XStringFormats.TopLeft);

            int linePerPageIndicator = 0;
            int currentPageIndicator = 0;
            for(int i = 0; i < numberOfLines; i++)
            {
                if(linePerPageIndicator == 40)
                {
                    currentPageIndicator++;
                    linePerPageIndicator = 0;
                    graph = XGraphics.FromPdfPage(pdfPages[currentPageIndicator]);
                }

                graph.DrawString(chatConsoleTextBox.Lines[i], font, XBrushes.Black,
                new XRect(0, (i - (40 * currentPageIndicator)) * (fontSize + 8), pdfPages[currentPageIndicator].Width.Point, pdfPages[currentPageIndicator].Height.Point), XStringFormats.TopLeft);
                linePerPageIndicator++;
            }  

            
            
            pdfDocument.Save("Server_Session_Log.pdf");            
        }

        private void StopSession_Click(object sender, EventArgs e) {
            if(searchingForClientsThread != null) {
                try {
                    ServerSocket.Stop();
                    searchingForClientsThread.Abort();
                }
                catch(Exception ex) {
                    // do loga
                }

                Thread.Sleep(500); // czekanie, aż się zamknie

                if (!searchingForClientsThread.IsAlive || true) {
                    // sesja zakończona, można zapisywać raport i pliki
                    using (var db = new LiteDatabase(databaseName)) {
                        var files = db.GetCollection<Model.ResultsRepository>();

                        foreach(var filename in ChangedFiles) {
                            var file = files.Find(x => x.FileName == filename).OrderBy(x => x.LastModified).FirstOrDefault();
                            var path = $"../../Zbior_testowy/{filename}";
                            if (file != null) {
                                // już był edytowany
                                path = $"{PathToRepo}{file.ActualFileName}";
                            }

                            var splittedModel = ModelData[filename].Split('\n');
                            var physicalFile = "";
                            using (var stream = File.OpenText(path)) {
                                physicalFile = stream.ReadToEnd();
                            }
                            var splittedFile = physicalFile.Split('\n');
                            ResultsRepository rr = new ResultsRepository();

                            rr.FileName = filename;
                            rr.LastModified = DateTime.Now;
                            rr.NewLines = new Dictionary<int, string>();
                            rr.OldLines = new Dictionary<int, string>();

                            try {
                                for(int i = 0; i < splittedFile.Length; i++) {
                                    if(splittedFile[i] != splittedModel[i]) {
                                        rr.OldLines.Add(i, splittedFile[i]);
                                        rr.NewLines.Add(i, splittedModel[i]);
                                    }
                                }   
                            }
                            catch(Exception ex) {
                                log.Error(ex);
                            }

                            string date = DateTime.Now.ToString("d.M.yyyy.HH.mm.ss");

                            string newName = $"{filename}_{date}";
                            rr.ActualFileName = newName;
                            
                            path = $"{PathToRepo}{newName}";

                            try {
                                if (!File.Exists(path))
                                    using (var stream = File.Create(path)) { } ;

                                using (var sw = new StreamWriter(path, false)) {
                                    sw.Write(ModelData[filename]);
                                }
                            }
                            catch (Exception ex) {
                                log.Error(ex);
                                continue;
                            }

                            files.Insert(rr);

                        }
                    }
                }
            }
        }
    }
}
